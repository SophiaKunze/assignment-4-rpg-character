# RPG Character

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![Java](https://img.shields.io/badge/-Java-red?logo=java)](https://www.java.com)

This is a Java console application built with Gradle and developed in continuous integration (CI).

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

The implementation of roll play game (RPG) characters is an assignment in the scope of a fullstack development 
course by [Noroff](https://www.noroff.no/en/). Four different Characters (Mage, Rogue, Ranger, Warrior) can be
created and equipped with different Iems (Weapons and Armor). The Characters equipment 
influences their damage per second (DPS).

## Install

This project was generated with OpenJDK version 17.0.3 and Gradle build system. Clone repository via `git clone`.


## Maintainers

[@SophiaKunze](https://gitlab.com/SophiaKunze)

## Contributing

Feel free to dive in! [Open an issue](https://gitlab.com/SophiaKunze/assignment-4-rpg-character/-/issues/new). This projects follows the [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) Code of Conduct.
Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

### Acknowledgement
This project exists thanks to my teacher <a href="https://gitlab.com/NicholasLennox">Nicholas Lennox</a> and <a href=https://www.experis.de/de>Experis</a>.

## License

MIT © 2022 Sophia Kunze
