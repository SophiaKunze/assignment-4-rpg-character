package de.experis.equipment;

import de.experis.characters.*;
import de.experis.characters.Character;
import de.experis.items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EquipmentTest {
    String testName = "test name";

    // test for equipping Weapons
    @Test
    public void equipWeapon_warriorEquippingLevel2Axe_shouldThrowInvalidWeaponException() {
        // Arrange
        Character testCharacter = new Warrior(testName);
        Weapon testWeapon = new Weapon(testName, 2, WeaponType.AXE, 7, 1.1);
        String expected = "Level of character too low to equip this weapon";
        // Act & Assert
        Exception exception = assertThrows(InvalidWeaponException.class, () ->
                testCharacter.equipWeapon(testWeapon)
        );
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }

    @Test
    public void equipWeapon_warriorEquippingLevel1Bow_shouldThrowInvalidWeaponException() {
        // Arrange
        Character testCharacter = new Warrior(testName);
        Weapon testWeapon = new Weapon(testName, 1, WeaponType.BOW, 7, 1.1);
        String expected = "Invalid weapon type";
        // Act & Assert
        Exception exception = assertThrows(InvalidWeaponException.class, () ->
                testCharacter.equipWeapon(testWeapon)
        );
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }

    @Test
    public void equipWeapon_warriorEquippingLevel1Hammer_shouldReturnTrue() throws InvalidWeaponException {
        // Arrange
        Character testCharacter = new Warrior(testName);
        Weapon testWeapon = new Weapon(testName, 1, WeaponType.HAMMER, 7, 1.1);
        boolean expected = true;
        // Act
        boolean actual = testCharacter.equipWeapon(testWeapon);
        // Assert
        assertEquals(expected, actual);
    }

    // tests for equipping Armor
    @Test
    void equipArmor_warriorEquippingLevel2PlateBody_shouldReturnInvalidArmorException() {
        // Arrange
        Character testCharacter = new Warrior(testName);
        Armor testArmor = new Armor(testName, 2, ArmorType.PLATE, new PrimaryAttribute(2,0,0));
        String expected = "Level of character too low to equip this armor";
        // Act & Assert
        Exception exception = assertThrows(InvalidArmorException.class, () ->
                testCharacter.equipArmor(testArmor, Slot.BODY));
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_warriorEquippingLevel1ClothHead_shouldReturnInvalidArmorException() {
        // Arrange
        Character testCharacter = new Warrior(testName);
        Armor testArmor = new Armor(testName, 1, ArmorType.CLOTH, new PrimaryAttribute(2,0,0));
        String expected = "Invalid armor type";
        // Act & Assert
        Exception exception = assertThrows(InvalidArmorException.class, () ->
                testCharacter.equipArmor(testArmor, Slot.HEAD));
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_warriorEquippingLevel1PlateBody_shouldReturnTrue() throws InvalidArmorException {
        // Arrange
        Character testCharacter = new Warrior(testName);
        Armor testArmor = new Armor(testName, 1, ArmorType.PLATE, new PrimaryAttribute(2,0,0));
        boolean expected = true;
        // Act
        boolean actual = testCharacter.equipArmor(testArmor, Slot.BODY);
        // Assert
        assertEquals(expected, actual);
    }

    // OPTIONAL test for total primary attribute
    @Test
    public void getTotalPrimaryAttribute_warriorWithoutEquipment_shouldReturnValidAttributes() {
        // Arrange
        Character testCharacter = new Warrior(testName);
        PrimaryAttribute expected = new PrimaryAttribute(5, 2, 1);
        // Act
        PrimaryAttribute actual = testCharacter.getTotalPrimaryAttribute();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getTotalPrimaryAttribute_warriorEquippedWithPlateBodyArmor_shouldReturnValidAttributes() throws InvalidArmorException {
        // Arrange
        Character testCharacter = new Warrior(testName);
        Armor testPlate = new Armor(testName, 1, ArmorType.PLATE, new PrimaryAttribute(2,2,2));
        testCharacter.equipArmor(testPlate, Slot.BODY);
        PrimaryAttribute expected = new PrimaryAttribute(7, 4, 3);
        // Act
        PrimaryAttribute actual = testCharacter.getTotalPrimaryAttribute();
        // Assert
        assertEquals(expected, actual);
    }

    // test for calculating Character's DPS
    @Test
    public void calculateDPS_warriorWithoutEquipment_shouldReturnCorrectDPS() {
        // Arrange
        Character testCharacter = new Warrior(testName);
        double expected = 1 * (1 + 5 / 100);
        // Act
        double actual = testCharacter.calculateDPS();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void calculateDPS_warriorValidWeaponEquipped_shouldReturnCorrectDPS() throws InvalidWeaponException {
        // Arrange
        Character testCharacter = new Warrior(testName);
        Weapon testWeapon = new Weapon(testName,1,WeaponType.AXE,7,1.1);
        testCharacter.equipWeapon(testWeapon);
        double expected = (7 * 1.1) * (1 + 5 / 100);
        // Act
        double actual = testCharacter.calculateDPS();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void calculateDPS_warriorValidWeaponArmorEquipped_shouldReturnCorrectDPS() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        Character testCharacter = new Warrior(testName);
        Weapon testWeapon = new Weapon(testName,1,WeaponType.AXE,7,1.1);
        Armor testArmor = new Armor(testName, 1, ArmorType.PLATE, new PrimaryAttribute(1,0,0));
        testCharacter.equipWeapon(testWeapon);
        testCharacter.equipArmor(testArmor, Slot.BODY);
        double expected = (7 * 1.1) * (1 + (5 + 1) / 100);
        // Act
        double actual = testCharacter.calculateDPS();
        // Assert
        assertEquals(expected, actual);
    }
}
