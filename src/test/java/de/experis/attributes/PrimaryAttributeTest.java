package de.experis.attributes;

import de.experis.characters.PrimaryAttribute;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PrimaryAttributeTest {

    // OPTIONAL test for primary attributes
    @Test
    public void equals_ofTwoPrimaryAttributeInstancesWithIdenticalFields_shouldReturnTrue() {
        // Arrange
        Boolean expected = true;
        PrimaryAttribute attributeOne = new PrimaryAttribute(1,1,1);
        PrimaryAttribute attributeTwo = new PrimaryAttribute(1,1,1);
        // Act
        Boolean actual = attributeOne.equals(attributeTwo);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void equals_ofTwoPrimaryAttributeInstancesWithDifferentStrength_shouldReturnFalse() {
        // Arrange
        Boolean expected = false;
        PrimaryAttribute attributeOne = new PrimaryAttribute(1,1,1);
        PrimaryAttribute attributeTwo = new PrimaryAttribute(2,1,1);
        // Act
        Boolean actual = attributeOne.equals(attributeTwo);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void equals_ofTwoPrimaryAttributeInstancesWithDifferentDexterity_shouldReturnFalse() {
        // Arrange
        Boolean expected = false;
        PrimaryAttribute attributeOne = new PrimaryAttribute(1,1,1);
        PrimaryAttribute attributeTwo = new PrimaryAttribute(1,2,1);
        // Act
        Boolean actual = attributeOne.equals(attributeTwo);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void equals_ofTwoPrimaryAttributeInstancesWithDifferentIntelligence_shouldReturnFalse() {
        // Arrange
        Boolean expected = false;
        PrimaryAttribute attributeOne = new PrimaryAttribute(1,1,1);
        PrimaryAttribute attributeTwo = new PrimaryAttribute(1,1,2);
        // Act
        Boolean actual = attributeOne.equals(attributeTwo);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void equals_ofTwoPrimaryAttributeInstancesWithAllFieldsDifferent_shouldReturnFalse() {
        // Arrange
        Boolean expected = false;
        PrimaryAttribute attributeOne = new PrimaryAttribute(1,1,1);
        PrimaryAttribute attributeTwo = new PrimaryAttribute(2,2,2);
        // Act
        Boolean actual = attributeOne.equals(attributeTwo);
        // Assert
        assertEquals(expected, actual);
    }

}
