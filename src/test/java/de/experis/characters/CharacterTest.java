package de.experis.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CharacterTest {

    String testName = "test name";

    // a character must be level 1 when created
    @Test
    public void getLevel_afterCreationOfWarrior_shouldReturn1() {
        // Arrange
        int expected = 1;
        Warrior warrior = new Warrior(testName);
        // Act
        int actual = warrior.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    // each character must have the proper default values when created
    @Test
    public void getBasePrimaryAttribute_afterCreationOfMage_shouldReturnPrimaryAttributeWithProperAttributes() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(1, 1, 8);
        Mage mage = new Mage(testName);
        // Act
        PrimaryAttribute actual = mage.getBasePrimaryAttribute();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getBasePrimaryAttribute_afterCreationOfRanger_shouldReturnPrimaryAttributeWithProperAttributes() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(1, 7, 1);
        Ranger ranger = new Ranger(testName);
        // Act
        PrimaryAttribute actual = ranger.getBasePrimaryAttribute();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getBasePrimaryAttribute_afterCreationOfRogue_shouldReturnPrimaryAttributeWithProperAttributes() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(2, 6, 1);
        Rogue rogue = new Rogue(testName);
        // Act
        PrimaryAttribute actual = rogue.getBasePrimaryAttribute();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getBasePrimaryAttribute_afterCreationOfWarrior_shouldReturnPrimaryAttributeWithProperAttributes() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(5, 2, 1);
        Warrior warrior = new Warrior(testName);
        // Act
        PrimaryAttribute actual = warrior.getBasePrimaryAttribute();
        // Assert
        assertEquals(expected, actual);
    }

    // a character should be in level 2 after gaining initial level
    @Test
    public void levelUp_increaseWarriorLevelAfterInitialLevel1_levelShouldBe2() {
        // Arrange
        int expected = 2;
        Warrior warrior = new Warrior(testName);
        warrior.levelUp();
        // Act
        int actual = warrior.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    // each character class has their attributes increased when leveling up
    @Test
    public void levelUp_increaseMageLevelAfterInitialLevel1_attributesShouldIncreaseCorrectly() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(2,2,13);
        Mage mage = new Mage(testName);
        mage.levelUp();
        // Act
        PrimaryAttribute actual = mage.getBasePrimaryAttribute();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelUp_increaseRangerLevelAfterInitialLevel1_attributesShouldIncreaseCorrectly() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(2,12,2);
        Ranger ranger = new Ranger(testName);
        ranger.levelUp();
        // Act
        PrimaryAttribute actual = ranger.getBasePrimaryAttribute();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelUp_increaseRogueLevelAfterInitialLevel1_attributesShouldIncreaseCorrectly() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(3,10,2);
        Rogue rogue = new Rogue(testName);
        rogue.levelUp();
        // Act
        PrimaryAttribute actual = rogue.getBasePrimaryAttribute();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelUp_increaseWarriorLevelAfterInitialLevel1_attributesShouldIncreaseCorrectly() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(8,4,2);
        Warrior warrior = new Warrior(testName);
        warrior.levelUp();
        // Act
        PrimaryAttribute actual = warrior.getBasePrimaryAttribute();
        // Assert
        assertEquals(expected, actual);
    }
}