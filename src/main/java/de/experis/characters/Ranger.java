package de.experis.characters;

import de.experis.items.ArmorType;
import de.experis.items.WeaponType;

import java.util.Arrays;

public class Ranger extends Character {

    // constructor
    public Ranger(String name) {
        super(
                name,
                new PrimaryAttribute(1, 7, 1),
                new PrimaryAttribute(1, 7, 1),
                new PrimaryAttribute(1,5,1),
                7,
                Arrays.asList(WeaponType.BOW),
                Arrays.asList(ArmorType.LEATHER, ArmorType.MAIL));
    }

    public int getTotalMainPrimaryAttribute() {
        // main attribute of Ranger is dexterity
        return this.getTotalPrimaryAttribute().getDexterity();
    }
}
