package de.experis.characters;

public enum AttributeType {
    STRENGTH,
    DEXTERITY,
    INTELLIGENCE
}
