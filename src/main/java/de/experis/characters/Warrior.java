package de.experis.characters;

import de.experis.items.ArmorType;
import de.experis.items.WeaponType;

import java.util.Arrays;

public class Warrior extends Character {

    // constructor
    public Warrior(String name) {
        super(
                name,
                new PrimaryAttribute(5, 2, 1),
                new PrimaryAttribute(5, 2, 1),
                new PrimaryAttribute(3,2,1),
                5,
                Arrays.asList(WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD),
                Arrays.asList(ArmorType.MAIL, ArmorType.PLATE));
    }

    // getter methods
    public int getTotalMainPrimaryAttribute() {
        // main attribute of Warrior is strength
        return this.getTotalPrimaryAttribute().getStrength();
    }
}
