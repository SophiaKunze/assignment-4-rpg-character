package de.experis.characters;

import de.experis.items.*;

import java.util.HashMap;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

public abstract class Character {
    // fields
    private final String name;
    private PrimaryAttribute basePrimaryAttribute; // defined by level and what Character's child is initialized
    private PrimaryAttribute totalPrimaryAttribute; // defined by Character's basePrimaryAttribute and equipped Armor
    private final PrimaryAttribute gainingPrimaryAttribute; // how Character's PrimaryAttributes increase by levelUp()
    private final int totalMainPrimaryAttribute; // power of Character's main attribute
    private final List<WeaponType> validWeapons; // types of weapons Character can equip
    private final List<ArmorType> validArmor; // types of armor Character can equip
    private HashMap<Slot, Item> equipment = new HashMap<>(); // equipped weapons and armor
    private int level = 1; // initial level

    // constructor
    public Character(String name, PrimaryAttribute basePrimaryAttribute, PrimaryAttribute totalPrimaryAttribute,
                     PrimaryAttribute gainingPrimaryAttribute, int totalMainPrimaryAttribute,
                     List<WeaponType> validWeapons, List<ArmorType> validArmor) {
        this.name = name;
        this.basePrimaryAttribute = basePrimaryAttribute;
        this.totalPrimaryAttribute = totalPrimaryAttribute;
        this.totalMainPrimaryAttribute = totalMainPrimaryAttribute;
        this.gainingPrimaryAttribute = gainingPrimaryAttribute;
        this.validWeapons = validWeapons;
        this.validArmor = validArmor;
    }

    // methods
    public void levelUp() {
        // increase level
        this.level += 1;
        // increase base primary attributes
        this.basePrimaryAttribute.increaseAttributes(
                this.gainingPrimaryAttribute.getStrength(),
                this.gainingPrimaryAttribute.getDexterity(),
                this.gainingPrimaryAttribute.getIntelligence()
        );
    }

    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (!this.validWeapons.contains(weapon.getType())) {
            throw new InvalidWeaponException("Invalid weapon type");
        }
        if (this.level < weapon.getRequiredLevel()) {
            throw new InvalidWeaponException("Level of character too low to equip this weapon");
        }
        this.equipment.put(Slot.WEAPON, weapon);
        return true;
    }

    public boolean equipArmor(Armor armor, Slot slot) throws InvalidArmorException {
        if (!this.validArmor.contains(armor.getType())) {
            throw new InvalidArmorException("Invalid armor type");
        }
        if (this.level < armor.getRequiredLevel()) {
            throw new InvalidArmorException("Level of character too low to equip this armor");
        }
        this.equipment.put(slot, armor);
        this.updateTotalPrimaryAttribute();
        return true;
    }

    private void updateTotalPrimaryAttribute() {
        // collect equipped armor
        ArrayList<Armor> equippedArmors = this.equipment.entrySet().stream()
                // filter for slots that can equip armor
                .filter(entry -> entry.getKey() != Slot.WEAPON)
                // receive values of HasMap entries
                .map(Map.Entry::getValue)
                // cast type of armor from Item to Armor
                .map(armor -> (Armor) armor)
                // collect all armor in ArrayList
                .collect(Collectors.toCollection(ArrayList::new));

        // receive strength by armor
        int armorStrength = equippedArmors.stream()
                // map strength of PrimaryAttribute
                .map(armor -> armor.getBonusAttributes().getStrength())
                // reduce to sum of all strength
                .reduce(0, Integer::sum);

        // receive dexterity by armor
        int armorDexterity = equippedArmors.stream()
                // map dexterity of PrimaryAttribute
                .map(armor -> armor.getBonusAttributes().getDexterity())
                // reduce to sum of all dexterity
                .reduce(0, Integer::sum);

        // receive intelligence by armor
        int armorIntelligence = equippedArmors.stream()
                // map intelligence of PrimaryAttribute
                .map(armor -> armor.getBonusAttributes().getIntelligence())
                // reduce to sum of all intelligence
                .reduce(0, Integer::sum);

        // calculate three values for total primary attribute
        int strength = armorStrength + this.basePrimaryAttribute.getStrength();
        int dexterity = armorDexterity + this.basePrimaryAttribute.getDexterity();
        int intelligence = armorIntelligence + this.basePrimaryAttribute.getIntelligence();

        // update field for total primary attribute
        this.totalPrimaryAttribute = new PrimaryAttribute(strength, dexterity, intelligence);
    }

    public double calculateDPS() {
        double weaponDPS;
        if (!this.equipment.containsKey(Slot.WEAPON)) {
            // weaponDPS defaults to zero if no weapon equipped
            weaponDPS = 1;
        } else {
            // cast type Item of equipped weapon into type Weapon and store in weapon
            Weapon weapon = ((Weapon) this.equipment.get(Slot.WEAPON));
            weaponDPS = weapon.getDamagePerSecond();
        }
        // calculate Character's DPS with main primary attribute
        return weaponDPS * (1 + this.getTotalMainPrimaryAttribute() / 100);
    }

    public void displayStats() {
        // logging of character stats to the console
        StringBuilder output = new StringBuilder();
        output.append("Name: " + this.name + "\n");
        output.append("Level: " + this.level + "\n");
        output.append("Strength: " + this.totalPrimaryAttribute.getStrength() + "\n");
        output.append("Dexterity: " + this.totalPrimaryAttribute.getDexterity() + "\n");
        output.append("Intelligence: " + this.totalPrimaryAttribute.getIntelligence() + "\n");
        output.append("DPS: " + this.calculateDPS());
        System.out.println(output);
    }

    // getter methods
    public int getLevel() {
        return level;
    }

    public PrimaryAttribute getBasePrimaryAttribute() {
        return basePrimaryAttribute;
    }

    public PrimaryAttribute getTotalPrimaryAttribute() {
        return totalPrimaryAttribute;
    }

    public abstract int getTotalMainPrimaryAttribute(); // child defines which one out of three is main primaryAttribute

}
