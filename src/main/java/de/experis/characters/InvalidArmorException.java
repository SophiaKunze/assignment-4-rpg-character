package de.experis.characters;

public class InvalidArmorException extends Exception{
    public InvalidArmorException(String message) {
        super(message);
    }
}
