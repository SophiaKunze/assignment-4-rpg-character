package de.experis.characters;

import de.experis.items.ArmorType;
import de.experis.items.WeaponType;

import java.util.Arrays;

public class Mage extends Character {

    // constructor
    public Mage(String name) {
        super(
                name,
                new PrimaryAttribute(1, 1, 8),
                new PrimaryAttribute(1, 1, 8),
                new PrimaryAttribute(1,1,5),
                8,
                Arrays.asList(WeaponType.STAFF, WeaponType.WAND),
                Arrays.asList(ArmorType.CLOTH));
    }

    // getter methods
    public int getTotalMainPrimaryAttribute() {
        // main attribute of Mage is intelligence
        return this.getTotalPrimaryAttribute().getIntelligence();
    }
}
