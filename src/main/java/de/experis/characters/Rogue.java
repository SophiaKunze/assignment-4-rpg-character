package de.experis.characters;

import de.experis.items.ArmorType;
import de.experis.items.WeaponType;

import java.util.Arrays;

public class Rogue extends Character {

    // constructor
    public Rogue(String name) {
        super(
                name,
                new PrimaryAttribute(2, 6, 1),
                new PrimaryAttribute(2, 6, 1),
                new PrimaryAttribute(1,4,1),
                6,
                Arrays.asList(WeaponType.DAGGER, WeaponType.SWORD),
                Arrays.asList(ArmorType.LEATHER, ArmorType.MAIL));
    }

    // getter methods
    public int getTotalMainPrimaryAttribute() {
        // main attribute of Rogue is dexterity
        return this.getTotalPrimaryAttribute().getDexterity();
    }
}
