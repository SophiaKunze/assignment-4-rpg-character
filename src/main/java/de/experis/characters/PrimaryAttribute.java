package de.experis.characters;

public class PrimaryAttribute {

    // fields
    private int strength;
    private int dexterity;
    private int intelligence;

    // constructor
    public PrimaryAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    // methods
    public void increaseAttributes(int strength, int dexterity, int intelligence) {
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass())
            return false;
        return ((PrimaryAttribute) obj).strength == this.strength
                && ((PrimaryAttribute) obj).dexterity == this.dexterity
                && ((PrimaryAttribute) obj).intelligence == this.intelligence;
    }

    // getter methods
    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }
}