package de.experis.items;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Item {
    // fields
    private final String name;

    private final int requiredLevel;
    private final List<Slot> possibleSlots = Arrays.asList(Slot.WEAPON);

    public Item(String name, int requiredLevel) {
        this.name = name;
        this.requiredLevel = requiredLevel;
    }

    // getter methods
    public int getRequiredLevel() {
        return requiredLevel;
    }
}