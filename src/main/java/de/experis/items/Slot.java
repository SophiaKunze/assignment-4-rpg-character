package de.experis.items;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
