package de.experis.items;

import de.experis.characters.PrimaryAttribute;

public class Armor extends Item {
     private final ArmorType type;
     private PrimaryAttribute bonusAttributes; // increases Character's totalPrimaryAttribute when equipped

    public Armor(String name, int requiredLevel, ArmorType type, PrimaryAttribute bonusAttributes) {
        super(name, requiredLevel);
        this.type = type;
        this.bonusAttributes = bonusAttributes;
    }

    // getter methods
    public ArmorType getType() {
        return type;
    }

    public PrimaryAttribute getBonusAttributes() {
        return bonusAttributes;
    }
}
