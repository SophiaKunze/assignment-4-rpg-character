package de.experis.items;

public class Weapon extends Item {

    private final WeaponType type;
    private final double damagePerSecond;

    public Weapon(String name, int requiredLevel, WeaponType type, int damage,
                  double attackSpeed) {
        super(name, requiredLevel);
        this.type = type;
        this.damagePerSecond = damage * attackSpeed;
    }

    // getter methods
    public WeaponType getType() {
        return type;
    }
    public double getDamagePerSecond() {
        return damagePerSecond;
    }
}
