package de.experis.items;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
