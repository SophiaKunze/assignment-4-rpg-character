package de.experis;

import de.experis.characters.*;
import de.experis.characters.Character;
import de.experis.items.*;

public class Main {
    public static void main(String[] args) {
        Character testCharacter = new Warrior("Sophia");
        Weapon testWeapon = new Weapon("Axy-Axe",1, WeaponType.AXE,7,1.1);
        Armor testArmor = new Armor("Protector-5000", 1, ArmorType.PLATE, new PrimaryAttribute(1,0,0));
        try {
            testCharacter.equipWeapon(testWeapon);
        }
        catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }
        try {
            testCharacter.equipArmor(testArmor, Slot.BODY);
        }
        catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
        testCharacter.displayStats();
    }
}